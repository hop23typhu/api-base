const express = require('express');

const router = express.Router();

const UserCtrl = require('../controllers/user');

router.route('/')
    .get(UserCtrl.get_all)
    .post(UserCtrl.create);

router.route('/:userId')
    .get(UserCtrl.get_one)
    .patch(UserCtrl.update)
    .delete(UserCtrl.delete)

module.exports = router;