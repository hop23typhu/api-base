'use-strict';
const express = require('express');
const logger = require('morgan');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
//import routes
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/demo');


const app = express();

const user = require('./routes/user');

//Middlewares
app.use(logger('dev'));
app.use(bodyParser.json());

//Routes

app.use('/user', user);


//Catch 404 erros amd forward to hanle function
app.use(function (req, res, next) {
    const err = new Error('Not found');
    err.status = 404;
    next(err);
});

//Error hander function
app.use(function (err, req, res, next) {
    const error = app.get('env') === 'development' ? err : {};
    const status = err.status || 500;

    //respond to client
    res.status(status).json({
        message: err.message
    })
    //respond to ourselves
    console.log(err, 'error');
});

//Start the server
const PORT = app.get('PORT') || 5001;
app.listen(PORT, function () {
    console.log(`App listening on port ${PORT}!`);
});
