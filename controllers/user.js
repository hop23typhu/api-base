const User = require('../models/user');

module.exports = {
    get_all: (req, res, next) => {
        User.find({})
            .then(user => res.status(200).json(user))
            .catch(err => next(err));
    },
    get_one: (req, res, next) => {
        let { userId } = req.params;
        User.findById(userId)
            .then(user => res.status(200).json(user))
            .catch(err => next(err));

    },
    create: (req, res, next) => {
        let new_user = new User(req.body);
        new_user.save()
            .then(user => res.status(201).json({ message: 'Add new user successfully' }))
            .catch(err => next(err));
    },
    update: (req, res, next) => {
        let { userId } = req.params;
        let new_user = req.body;
        console.log(new_user, 'new_user new_user');
        User.findByIdAndUpdate(userId, new_user)
            .then(user => res.status(201).json({ success: true, id: user._id }))
            .catch(err => next(err));
    },
    delete: (req, res, next) => {
        let { userId } = req.params;
        User.findByIdAndRemove(userId)
            .then(user => res.status(201).json({ message: 'Remove successfully' }))
            .catch(err => next(err));
    }

}